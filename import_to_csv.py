from utils import client, tz_from, tz_to, moscow_tz


def insert_positions():
    response = client.get_portfolio()
    positions = response.payload.positions

    csv_rows = [",".join(["name",
                          "average_position_price",
                          "currency",
                          "balance",
                          "expected_yeild",
                          "ticker",
                          "figi",
                          "instrument_type"])]

    for position in positions:
        if "," in position.name:
            position.name = position.name.replace(",", '.')

        csv_rows.append(",".join(map(str, [position.name,
                                           position.average_position_price.value,
                                           position.average_position_price.currency.value,
                                           position.balance,
                                           position.expected_yield.value,
                                           position.ticker,
                                           position.figi,
                                           position.instrument_type.value
                                           ])))

    with open('positions.csv', 'w') as f:
        f.write("\n".join(csv_rows))
        print("OK")


def insert_operations():
    response = client.get_operations(tz_from(moscow_tz), tz_to(moscow_tz))
    operations = response.payload.operations

    csv_rows = [",".join(["date",
                          "comission_value",
                          "currency",
                          "figi",
                          "instrument_type",
                          "operation_type",
                          "payment",
                          "price",
                          "quantity",
                          "status"
                          ])]
    for operation in operations:
        csv_rows.append(",".join(map(str, [
            operation.date,
            operation.commission.value if operation.commission else '',
            operation.currency.value if operation.currency else '',
            operation.figi,
            operation.instrument_type.value if operation.instrument_type else '',
            operation.operation_type.value,
            operation.payment,
            operation.price or '',
            operation.quantity or '',
            operation.status.value
        ])))

        with open("operations.csv", "w") as f:
            f.write("\n".join(csv_rows))


if __name__ == '__main__':
    insert_positions()
    insert_operations()
