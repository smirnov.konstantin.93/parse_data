import tinvest
import os

from pycbrf.toolbox import ExchangeRates
from datetime import datetime
from pytz import timezone


TOKEN = os.getenv("TINVEST_TOKEN")
client = tinvest.SyncClient(TOKEN)


def get_usd_course(tz):
    rates = ExchangeRates(get_now(tz))
    return rates['USD'].value


def get_eur_course(tz):
    rates = ExchangeRates(get_now(tz))
    return rates['EUR'].value


def get_now(tz):
    return tz.localize(datetime.now()).strftime("%Y-%m-%d")


def tz_from(tz):
    return tz.localize(datetime(2020, 1, 1, 0, 0, 0))


def tz_to(tz):
    return tz.localize(datetime.now())


moscow_tz = timezone('Europe/Moscow')
usd_course = get_usd_course(moscow_tz)
eur_course = get_eur_course(moscow_tz)